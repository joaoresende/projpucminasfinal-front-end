import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login/Form'
import FornecedorListar from '@/components/Fornecedor/Listar'
import FornecedorView from '@/components/Fornecedor/Visualizar'
import FornecedorCreate from '@/components/Fornecedor/Cadastrar'
import FornecedorEdit from '@/components/Fornecedor/Editar'
import ProdutoListar from '@/components/Produto/Listar'
import ProdutoCadastrar from '@/components/Produto/Cadastrar'
import ProdutoEditar from '@/components/Produto/Editar'
import ProdutoView from '@/components/Produto/Visualizar'

const routes = [
  {
    path: '/',
    name: 'HelloWorld',
    meta: { siderBar: false },
    component: HelloWorld
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/fornecedores',
    name: 'FornecedorListar',
    component: FornecedorListar
  },
  {
    path: '/fornecedores/novo',
    name: 'FornecedorCreate',
    component: FornecedorCreate
  },
  {
    path: '/fornecedores/:id/editar',
    name: 'FornecedorEdit',
    component: FornecedorEdit
  },
  {
    path: '/fornecedores/:id',
    name: 'FornecedorView',
    component: FornecedorView
  },
  {
    path: '/produtos',
    name: 'ProdutoListar',
    component: ProdutoListar
  },
  {
    path: '/produtos/novo',
    name: 'ProdutoCadastrar',
    component: ProdutoCadastrar
  },
  {
    path: '/produtos/:id/editar',
    name: 'ProdutoEditar',
    component: ProdutoEditar
  },
  {
    path: '/produtos/:id',
    name: 'ProdutoView',
    component: ProdutoView
  }
]

export default routes
