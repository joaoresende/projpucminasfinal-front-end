import fornecedor from './modules/fornecedor'
import produto from './modules/produto'
import pagination from './modules/pagination'

export default {
  modules: {
    pagination: pagination,
    fornecedor: fornecedor,
    produto: produto
  }
}
