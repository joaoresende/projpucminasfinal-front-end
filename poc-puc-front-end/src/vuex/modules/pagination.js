import Vue from 'vue'

export default {
  state: {
    getList: [],
    getListLoading: false
  },
  mutations: {
    updateList (state, data) {
      state.getList = data
    },
    updateListLoading (state, boo) {
      state.getListLoading = boo
    }
  },
  actions: {
    clearRegistries (context, config) {
      context.commit('updateList', [])
    },
    getRegistries (context, config) {
      if (!config.page) {
        config.page = 1
      }
      if (!config.like) {
        config.like = ''
      }
      context.commit('updateListLoading', true)
      Vue.http.get('api/' + config.resource + '?page=' + config.page + '&like=' + config.like).then(response => {
        context.commit('updateList', response.data)
        context.commit('updateListLoading', false)
      })
    }
  }
}
