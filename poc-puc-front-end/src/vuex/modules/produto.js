import Vue from 'vue'

export default {
  state: {
    produtoView: {}
  },
  mutations: {
    updateProdutoView (state, data) {
      state.produtoView = data
    }
  },
  actions: {
    getProduto (context, id) {
      Vue.http.get('api/produto/' + id).then(response => {
        context.commit('updateProdutoView', response.data)
      })
    },
    createProduto (context, data) {
      Vue.http.post('api/produto', data)
    },
    updateProduto (context, params) {
      Vue.http.put('api/produto/' + params.id, params.data)
    },
    removeProduto (context, id) {
      Vue.http.delete('api/produto/' + id)
    }
  }
}
