import Vue from 'vue'

export default {
  state: {
    fornecedorView: {}
  },
  mutations: {
    updateFornecedorView (state, data) {
      state.fornecedorView = data
    },
    updateFornecedorListLoading (state, boo) {
      state.fornecedorListLoading = boo
    }
  },
  actions: {
    getFornecedor (context, id) {
      Vue.http.get('api/fornecedor/' + id).then(response => {
        context.commit('updateFornecedorView', response.data)
      })
    },
    createFornecedor (context, data) {
      Vue.http.post('api/fornecedor', data)
    },
    updateFornecedor (context, params) {
      Vue.http.put('api/fornecedor/' + params.id, params.data)
    },
    removeFornecedor (context, id) {
      Vue.http.delete('api/fornecedor/' + id)
    }
  }
}
